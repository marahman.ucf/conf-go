<!-- Improved compatibility of back to top link: See: https://github.com/othneildrew/Best-README-Template/pull/73 -->
<a name="readme-top"></a>


<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://gitlab.com/marahman.ucf/conf-go">
    <img src="ghi/app/public/icon.svg" alt="Logo" width="80" height="80">
  </a>

<h3 align="center">Conference Go</h3>

  <p align="center">
        Welcome to Conference Go – your all-in-one conference management solution!
    <br />
    <br />
  </p>
</div>



<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#acknowledgments">Acknowledgments</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Project

[![Project Name Screen Shot][project-screenshot1]](https://gitlab.com/marahman.ucf/conf-go)

[![Project Name Screen Shot][project-screenshot2]](https://gitlab.com/marahman.ucf/conf-go)

Effortlessly create conferences, add stunning city images from a third-party API to set the scene, manage attendee lists, and curate new presentations. Our user-friendly platform streamlines the entire process. The homepage presents all your upcoming conferences, providing a visual preview of each event. Simplify conference planning, enhance attendee experiences, and take your event management to the next level with Conference Go. Join us today and redefine the way you host and attend conferences!


### Built With

[![React][React.js]][React-url] &nbsp; [![ReactRouter][ReactRouter.com]][ReactRouter-url]

[![Docker][Docker.com]][Docker-url] &nbsp; [![Bootstrap][Bootstrap.com]][Bootstrap-url] &nbsp;

[![Django][Django.com]][Django-url] &nbsp; [![HTML5][HTML5.com]][HTML5-url]

[![Python][Python.org]][Python-url] &nbsp; [![Javascript][Javascript.com]][Javascript-url] &nbsp;



<!-- GETTING STARTED -->
## Getting Started

### Prerequisites

The installation instructions assume your system has the following software: [Google Chrome](https://www.google.com/chrome/) and [Docker](https://www.docker.com/).

If you don't have these (or equivalent) software, please install them before proceeding.

To get a local copy of Conference Go up and running on your machine follow these simple steps.


### Installation

1. Clone the [repository](https://gitlab.com/marahman.ucf/conf-go)

2. Run `docker compose build`

3. Run `docker compose up`

4. Navigate to [localhost:3001](http://localhost:3001/)



<!-- ACKNOWLEDGMENTS -->
## Acknowledgments

[React](https://react.dev/) · [React Router](https://reactrouter.com/en/main)

[Docker](https://www.docker.com/) · [Bootstrap](https://getbootstrap.com/)

[Django](https://www.djangoproject.com/) · [HTML5](https://developer.mozilla.org/en-US/docs/Web/HTML)

[Python](https://www.python.org/) · [Javascript](https://developer.mozilla.org/en-US/docs/Web/JavaScript)

[Shields.io](https://shields.io/) · [Simple Icons](https://simpleicons.org/) · [Pexels](https://www.pexels.com/)

<p align="right">(<a href="#readme-top">back to top</a>)</p>



<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[project-screenshot1]: ghi/app/public/screenshot1.png
[project-screenshot2]: ghi/app/public/screenshot2.png


[React.js]: https://img.shields.io/badge/React-61DAFB?style=for-the-badge&logo=react&logoColor=white
[React-url]: https://reactjs.org/

[Bootstrap.com]: https://img.shields.io/badge/Bootstrap-7952B3?style=for-the-badge&logo=bootstrap&logoColor=white
[Bootstrap-url]: https://getbootstrap.com

[Docker.com]: https://img.shields.io/badge/Docker-2496ED?style=for-the-badge&logo=docker&logoColor=white
[Docker-url]: https://www.docker.com/

[HTML5.com]: https://img.shields.io/badge/HTML5-E34F26?style=for-the-badge&logo=html5&logoColor=white
[HTML5-url]: https://developer.mozilla.org/en-US/docs/Web/HTML

[Python.org]: https://img.shields.io/badge/Python-3776AB?style=for-the-badge&logo=python&logoColor=white
[Python-url]: https://www.python.org/

[Javascript.com]: https://img.shields.io/badge/JavaScript-F7DF1E?style=for-the-badge&logo=javascript&logoColor=white
[Javascript-url]: https://developer.mozilla.org/en-US/docs/Web/JavaScript

[ReactRouter.com]: https://img.shields.io/badge/React_Router-CA4245?style=for-the-badge&logo=reactrouter&logoColor=white
[ReactRouter-url]: https://reactrouter.com/en/main

[Django.com]: https://img.shields.io/badge/Django-092E20?style=for-the-badge&logo=django&logoColor=white
[Django-url]: https://www.djangoproject.com/
